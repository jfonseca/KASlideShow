# KASlideShow 1.2

This slideshow component was built during the flash days and converted into Javascript. 

## Example

To run the examples you can either upload 'Example' folder to your server or run it locally by using the npm library inside the 'Example' folder.

[View Example Online](https://www.klipartstudio.com/site/KAS_SlideShowBuilder/)


[NPM](https://www.npmjs.com/get-npm) - Install NPM if you currently don't have it

* Install the package from npm

`npm install`

* Start the local server

`npm start`

## Usage

* Jquery required

### DESCRIPTION:
* link      : XML URL
* width     : Banner Width
* height     : Banner height
* speed     : The speed the banner will change
* width     : Banner Width
* quality   : Amount of slices to produce
* effects   : Type of transition
* menu       : shows menu at bottom of banner

Javascript code:
```javascript
$('#rotator3').rotator({
    link:'content.xml', 
    width:600, 
    height:275, 
    speed:5, 
    quality:40, 
    effects:'randomFade', 
    menu:false});
```

XML code:
```xml
<?xml version="1.0"?>
<rotator>
    <banners>
        <jpg image="path" link="http://www.sitelinkforimage.com" targetLink="_self" head="" desc="" title=""/>
    </banners>
</rotator>
```

## Authors
* **Jose Fonseca** - *[Klip Art Studio](https://www.klipartstudio.com)* - [GIT](https://gitlab.com/jfonseca/KASlideShow)

Contributor [Ben Lister](http://www.DarkCrimson.com) who participated in this project.

## License

MIT
