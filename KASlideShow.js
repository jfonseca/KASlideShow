/*!
 * KASlideShow: 1.2
 * http://www.klipartstudio.com/
 *
 * Copyright 2010, Klip Art Studio, LLC
 *
 * TESTED ON JQUERY: 1.3.2
 * BROWSER SUPPORT: Safri 5.0.2, IE8, FF 3.6.10
 * Copyright 2010, The Dojo Foundation
 * 
 * DESCRIPTION: (This code was built in as3 then convert to javascript)
 *   link      : XML URL
 *   width     : Banner Width
 *   height     : Banner height
 *   speed     : The speed the banner will change
 *   width     : Banner Width
 *   quality   : Amount of slices to produce
 *   effects   : Type of transition
 *   menu       : shows menu at bottom of banner
 *
 * EFFECTS COMBINATIONS:
 *    If (quality : 1) and (effects:'fade') this will produce the common fade transition.
 *  
 * USAGE:
 *    $('#rotator3').rotator({link:'content.xml', width:600, height:275, speed:5, quality:40, effects:'randomFade', menu:false});
 *
 * AUTHOR: Jose Fonseca, josef@klipartstudio.com also assisted by Ben Lister from http://www.DarkCrimson.com
 * DATE: 10/28/2010
 */
(function($){
  $.fn.KASlideShow = function(options){
   //plugin name - animatemenu
    var defaults = {
    	menuH				 : 15,
    	menuColor    : 'transparent',
    	menuOnColor  : '#fff',
    	effectDelaySpeed: 10,
    	effectTime: 200,
    	autoStart: true,
    	quality  : 1,
    	slice		 : 1,
    	info 		 : true,
			width    : 450,
			height   : 300,
			target   : $(this),
			link     : '',
			speed    : 2,
			effects  : 'fade',
			random	 : false,
			imagePath: 'images/',
			type     : 'default',
			menu		 : true
    };
    var options = $.extend(defaults, options);
    return this.each(function () {
      (new $.KASlideShow($(this), options));
    });
  }			
				  
  $.KASlideShow = function(itemTarget, options){
    //plugin name - animatemenu
    
    itemTarget.data('KASlideShow', this);
    
    var p_obj = [];
    var obj = new Array(); //item
    var objTrans = new Array(); //item
    var tempArray = new Array();
    var panelW = 0;
    var panelH = 0;
    var linkItem = ""; //item link
    var itemId = 0; //item id
    var slideTime; //item interval
    var txtBoxSize = 50;
    var selectedMenu = '';
    var currFade = 1;
    var initRotator = false;
  
    var p_obj = options;
    
    p_obj.target = '#'+$(itemTarget).attr('id');
    
    if(p_obj.effectTime != options.effectTime){
    	p_obj.effectTime = p_obj.effectTime* 1000;
    }
    
    //Setting for Rotator
    var settings = function(){
      var buildHtml;
      currFade = currFade;
      
      panelW  	=	p_obj.width;
      panelH = p_obj.height;

      buildHtml =  '<div class="transit">';
      
      buildHtml += '<img class="loading" src="'+p_obj.imagePath+'loading.gif" alt="loading" />';
      buildHtml += '<div id="'+(p_obj.target+'_flash')+'" class="swfItem"></div>';
      buildHtml += '<div class="slide_main"></div>';
      buildHtml += '<div class="transition"></div>';
      buildHtml += '<div class="cornerRt"></div>';
      buildHtml += '<div class="cornerLt"></div>';
      buildHtml += '<div class="infoBar"></div>';
      buildHtml += '<div class="urlCall"></div>';
      buildHtml += '<div class="back"></div><div class="forward"></div>';
      buildHtml += '<ul class="menu"></ul>';
      buildHtml += '</div>';
      
      $(buildHtml).appendTo(p_obj.target);
  		
  		if($(p_obj.target + ' ul.menu').css('height') != '0px'){
  			//console.log($(p_obj.target + ' ul.menu').css('height'));
  			p_obj.menuH  = cleanString($(p_obj.target + ' ul.menu').css('height'));
  		}
      var panelMenu = panelH;
      
      css = {
        'display' : 'block',
        'position': 'absolute',
        'height'  : panelH,
        'width'   : panelW
      };
      $(p_obj.target + ' .loading').css(css);
      
      view = 'block';
      if(!p_obj.menu){
        var view = 'none';
        menuH = 0;
      }
      var overflow = 'hidden';
      if(p_obj.type != 'default'){
        overflow = 'visible';
      }
      css = {
        'overflow' : overflow,
        'height'   : panelMenu,
        'width'    : panelW
      };
      $(p_obj.target + ' .transit').css(css);
      
      
      css = {
        'overflow' : overflow,
        'height'    : panelMenu,
        'width'     : panelW
      };
      $(p_obj.target).css(css);
      
      css = {
        'position'        : 'absolute',
        'background-color': '#000000',
        'padding'      : '0 0 0 0',
        'margin'      : '0 0 0 0',
        'height'      : panelH,
        'width'        : panelW,
        'z-index'     : '3'
      };
      $(p_obj.target + ' .swfItem').css(css);
      
      css = {
        'position' : 'absolute',
        'padding'   : '0 0 0 0',
        'margin'   : '0 0 0 0',
        'height'   : panelH,
        'width'    : panelW,
        'z-index'  : '4'
      };
      $(p_obj.target + ' .slide_main').css(css);
      
      css = {
        'position' : 'absolute',
        'overflow' : 'hidden',
        'padding'   : '0 0 0 0',
        'margin'   : '0 0 0 0',
        'height'   : panelH,
        'width'     : panelW,
        'opacity'   : '0',
        'z-index'  : '5'
      };
      $(p_obj.target + ' .transition').css(css);
      
  		css = {
  			'display' : 'block',
        'position': 'absolute',
        'height'   : panelH,
        'width'     : panelW,
        'z-index'     : '6'
      };
      $(p_obj.target + ' .cornerRt').css(css);
      css = {
        'display' : 'block',
        'position': 'absolute',
        'height'   : panelH,
        'width'     : panelW,
        'z-index'     : '6'
      };
      $(p_obj.target + ' .cornerLt').css(css);
      
      
      var ieExtra = 5;
      if($.browser.msie){
        ieExtra = 0;
      }
      css = {
        'position'         : 'absolute',
        'overflow'          : 'hidden',
        'background-image' : 'url('+p_obj.imagePath+'transparent_85.png)',
        'background-repeat': 'repeat',
        'top' 				 : (panelH-txtBoxSize),
        'height'      : txtBoxSize,
        'width'        : panelW,
        'z-index'     : '6'
          
      };
      $(p_obj.target + ' .infoBar').css(css).fadeOut(0);;
      
      
      css = {
        'color'            :  '#fff',
        'background-color' : '#fff',
        'position'    : 'absolute',
        'cursor'      : 'pointer',
        'padding'     : '0 0 0 0',
        'margin'      : '0 0 0 0',
        'height'      : panelH,
        'width'       : panelW,
        'z-index'     : '7',
          'filter'    : 'alpha(opacity=0)',
          'opacity'   : '0'
      };
      $(p_obj.target + ' .urlCall').css(css);
  		
  		var menuPLaceY = eval(panelH - p_obj.menuH);
      css = {
      	'position' 			: 'relative',
        'display'       :  view,
        'width'         : panelW,
        'padding'       : 0,
        'height'				: p_obj.menuH,
        'top'						: menuPLaceY,
        'overflow'      : overflow,
        'text-overflow' : 'ellipsis',
        'z-index'     : '100'
      };
      $(p_obj.target + ' ul.menu').css(css);

			css = {
        'z-index'  : '8',
        'filter'    : 'alpha(opacity=0)',
          'opacity'   : '0'
      };
      $(p_obj.target + ' .back').css(css);  
      css = {
        'z-index'  : '9',
        'filter'    : 'alpha(opacity=0)',
          'opacity'   : '0'
      };
      $(p_obj.target + ' .forward').css(css);    
      loadInfo(p_obj.link);

      
    }
    
   var cleanString = function (str) {
		    return str.replace(/[^\d]/g, "");
		}
		
		var str_replace_reg = function(haystack, needle, replacement) {
			var r = new RegExp(needle, 'g');
			return haystack.replace(r, replacement);
		}
    //LOADS IMAGES
    var imgCall = function(file, id){
      if(file != "swf"){
        var loadCheck;
            
        if(obj[id].loaded == false){
          $(p_obj.target + ' .loading').fadeIn(100);
          obj[id].file = new Image();
          loadCheck = false;
        }else{
          loadCheck = obj[id].loaded;
        }
        if(loadCheck){
          //Already Loaded
          doneLoading(id);
        }else{
          //Load image for the first time
          obj[id].file.onload = function() {
            obj[id].loaded = true;
            doneLoading(id);
          }
          obj[id].file.src = file;
        }
        
      }
    }
    
    var loadInfo = function(urlCall){
      //THIS LOADS THE XML CALL
      $.ajax({
        type    : "GET",
        url      : urlCall,
        dataType: ($.browser.msie) ? "text" : "XML",
      success  : function(xml, status){
      	if($.browser.msie){
      		buildMenu(createXmlDOMObject(xml));
      	} else{
      		buildMenu(xml);
      	}
      	
      },
      complete: function( xhr, status )
      {
        if( status == 'parsererror' )
        {
          xml = null;
          if( window.DOMParser )
          {
            parser=new DOMParser();
            xml=parser.parseFromString( xhr.responseText,"text/xml" ) ;
          }
          else // Internet Explorer
          {
            xml=getIEXmlDom();
            xml.async = "false";
            xml.loadXML( xhr.responseText );
          }
          
          buildMenu( createXmlDOMObject(xml) );
        }
      },
      error: function( xhr, status, error )
      {
  /*
        alert( 'ERROR: ' + status ) ;
        alert( xhr.responseText ) ;
  */
      }
      });
    }
    var createXmlDOMObject = function (xmlString)
    {
        var xmlDoc = null;
        xmlString = jQuery.trim( xmlString )
        if( ! window.DOMParser )
        {
            // the xml string cannot be directly manipulated by browsers 
            // such as Internet Explorer because they rely on an external 
            // DOM parsing framework...
            // create and load an XML document object through the DOM 
            // ActiveXObject that it can deal with
            xmlDoc = getIEXmlDom();
            xmlDoc.async = false;
            xmlDoc.loadXML( xmlString );          
        }
        else
        {
            // the current browser is capable of creating its own DOM parser
            parser = new DOMParser();
            xmlDoc = parser.parseFromString( xmlString, "text/xml" ) ;
        }
  			//alert($(xmlString).find("jpg").length);
        return xmlDoc;
    }
    var getIEXmlDom = function() {
      // THIS WAS BUILT TO SUPPORT IE8 AND FUTURE LAUNCHES WORKING WITH XML
      var doc = null;
      var docStrings = [
          "Msxml2.DOMDocument.6.0",
          "Msxml2.DOMDocument.5.0",
          "Msxml2.DOMDocument.4.0",
          "MSXML2.DOMDocument.3.0",
          "MSXML2.DOMDocument",
          "MSXML.DOMDocument",
          "Microsoft.XMLDOM"
      ];
      for (var d = 0; d < docStrings.length; d++) {
        if (doc === null) {
          try {
            doc = new window.ActiveXObject(docStrings[d]);
            //alert('  doc Items  '  + docStrings[d]);
          } catch(e) {
          	
            doc = null;
          }
        } else {
          break;
        }
      }
      //alert(window.ActiveXObject + '  doc   '  + doc);
      return doc;
    }
  
    var xtractFile = function(data){
      //EXTRACTS THE FILE
      var m = data.match(/(.*)\/([^\/\\]+)(\.\w+)$/);
      return {path: m[1], file: m[2], extension: m[3]}
    }
    
    var buildMenu = function(xml){
      ///////////////////////////////////////////////////
      // BUILDS THE BOITTOM MENU ////////////////////////
      ///////////////////////////////////////////////////
      
      var i = 0;
      var count = 0;
      var imgObj = [];
			
      $(xml).find("jpg").each(function()
      {
        var fileType = xtractFile($(this).attr("image"));
        
        //STORES ALL DATA INTO AN OBJECT FOR FUTURE USE
        obj.push({
          id    : count, 
          type  : fileType.extension, 
          image : $(this).attr("image"), 
          link  : $(this).attr("link"), 
          target: $(this).attr("targetLink"), 
          title : $(this).attr("title"), 
          header: $(this).attr("head"), 
          desc  : $(this).attr("desc"), 
          loaded: false
        });
        count++;
        
      }); 
      
			if(p_obj.type == 'default'){
      	textMenu();
      }else{
      	imageMenu();
      }
      
      if(p_obj.effects == 'slide'){
      	transitionSlide();
      }else{
      	transition();
      }
      if(p_obj.autoStart){
      	start();
      }
    }
    
    var textMenu = function(){
      //CALCULATES THE MENU SIZE
      var slideCount = obj.length;
      var butSize = Math.round(panelW/(slideCount));
  		var styleName;
  
      for (counter = 0; counter <= slideCount-1; counter += 1) { 
      	styleName = 'menuli';
		    //alert(counter + '  -----  ' + (slideCount-1) + '  STYLE   ' + styleName);
      	if(counter != slideCount-1){
	         $(p_obj.target + ' ul.menu').append('<li class="'+styleName+'" selID="'+counter+'" style="width:'+butSize+'px; position: absolute; left:'+(butSize*counter)+'px;">'+obj[counter].title+'</li>');
      	} else{
      		var leftPx = panelW - (butSize*slideCount);
      		var totalPx = butSize + leftPx;
      		//console.log(totalPx + '  leftPx  ' + leftPx);
	        $(p_obj.target + ' ul.menu').append('<li class="'+styleName+'" selID="'+counter+'" style="width:'+totalPx+'px; position: absolute; left:'+(butSize*counter)+'px;">'+obj[counter].title+'</li>');
      	}

      }
      
      //CSS FOR MENU
      css = {
      	'height' 	 : eval(p_obj.menuH),
        'overflow' : 'hidden',
        'cursor'   : 'pointer'
      };
      $(p_obj.target + ' ul.menu li').css(css);
    }
    
    var imageMenu = function(){
      //CALCULATES THE MENU SIZE
      var slideCount = obj.length;
      var butSize = Math.round(panelW/(slideCount));
  		var styleName;
  
      for (counter = 0; counter <= slideCount-1; counter += 1) { 
      	styleName = 'menuli';
      	var thm = str_replace_reg(obj[counter].image, 'lrg', 'thm');
      	
		    //alert(counter + '  -----  ' + (slideCount-1) + '  STYLE   ' + styleName);
      	if(counter != slideCount-1){
	         $(p_obj.target + ' ul.menu').append('<li class="'+styleName+'" selID="'+counter+'" style="left:'+(butSize*counter)+'px;">'+'<img src="'+thm+'" />'+'</li>');
      	} else{
      		var leftPx = panelW - (butSize*slideCount);
      		var totalPx = butSize + leftPx;
      		//console.log(totalPx + '  leftPx  ' + leftPx);
	        $(p_obj.target + ' ul.menu').append('<li class="'+styleName+'" selID="'+counter+'" style="left:'+(butSize*counter)+'px;">'+'<img src="'+thm+'" />'+'</li>');
      	}

      }
      
      //CSS FOR MENU
      css = {
      	'height' 	 : eval(p_obj.menuH),
        'overflow' : 'hidden',
        'cursor'   : 'pointer'
      };
      $(p_obj.target + ' ul.menu li').css(css);
    }   

    
    var activeBut = function(item){
      ///////////////////////////////////////////////////
      // Menu Action ////////////////////////////////////
      ///////////////////////////////////////////////////
     $(item).hover(
        function(){
	  			var className = $(this).attr("class");
	        switch(className){
	        	case "forward":
	        	case "back":
		          css = {
			          'opacity': '1',
			          //'margin'      : (panelH+' 0 0 0'),
			          'filter' : 'alpha(opacity=100)'
			        }
			        $(p_obj.target + ' .back').stop().animate(css, 250);
	        		$(p_obj.target + ' .forward').stop().animate(css, 250);
	        	break;
	        	default:
			        css = {
			          'color':'#ffffff',
			          'background-color': '#000000'
			        };
			        //$(this).css(css);
	        	break;
	        }
          //console.log( 'OVER' );
        },
        function(){
	  			var className = $(this).attr("class");
	        switch(className){
	        	case "forward":
	        	case "back":
		          css = {
			          'opacity': '0',
			          //'margin'      : (panelH+' 0 0 0'),
			          'filter' : 'alpha(opacity=0)'
			        }
			        $(p_obj.target + ' .back').stop().animate(css, 1000);
	        		$(p_obj.target + ' .forward').stop().animate(css, 1000);
	        	break;
	        	default:
	        		//itemId = $(this).attr("selID");
			        css = {
			          'color':'#ffffff',
			          'background-color': '#000000'
			        };
			        //$(this).css(css);
	        	break;
	        }
  
        }
      );
      
      $(item).click(function () {
      	
        

  			var className = $(this).attr("class");
        switch(className){
        	case "forward":
        		itemId++;
        	break;
        	case "back":
        		itemId--;
        		if(itemId < 0){
        			itemId = obj.length-1;
        		}
        	break;
        	default:
        		itemId = $(this).attr("selID");
        	break;
        }
        if(itemId > obj.length-1){
          itemId = 0;
        }
        menuHighLight(itemId);
        clearTimeout(slideTime);
        fade(itemId, 'click');
      });
      
    }
    var rgb2hex = function(rgb) {
    	//alert(rgb);//background-color: transparent;
			if(rgb.search("rgba") != -1){
				return 'transparent';
			}else{
				if (  rgb.search("rgb") == -1 ) {
				  return rgb;
				} else {
				  rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
				  function hex(x) {
				       return ("0" + parseInt(x).toString(16)).slice(-2);
				  }
				  return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
				}
			}
		}
    var imagePanel = function (item){
      ///////////////////////////////////////////////////
      // CLICK STATE FOR CURRENT ITEM DISPLAYED   ///////
      ///////////////////////////////////////////////////
      
      $(item).hover(
        function(){
          if(!p_obj.info){
            textInfo('on');
          }
          css = {
	          'opacity': '1',
	          'filter' : 'alpha(opacity=100)'
	        }
          $(p_obj.target + ' .back').stop().animate(css, 250);
	        $(p_obj.target + ' .forward').stop().animate(css, 250);
          //console.log( 'OVER' );
        },
        function(){
          if(!p_obj.info){
            textInfo('off');
          }
          css = {
	          'opacity': '0',
	          'filter' : 'alpha(opacity=0)'
	        }
	        $(p_obj.target + ' .back').stop().animate(css, 1000);
	        $(p_obj.target + ' .forward').stop().animate(css, 1000);
          //console.log( "mouseLeave" );
        }
      );
 
      
      $(item).click(function () { 
          window.open(linkItem, '_blank');
      });
    }
    this.start = function(){
    //console.log('start');
    	if(!initRotator){
    		start();
    	}
    }
   
    var start = function(){
      ///////////////////////////////////////////////////
      // START PLUGIN   /////////////////////////////////
      ///////////////////////////////////////////////////
      activeBut(p_obj.target + ' li');
      activeBut(p_obj.target + ' .back');
      activeBut(p_obj.target + ' .forward');
      
      imagePanel(p_obj.target + ' .urlCall');
      if(p_obj.random){
      	itemId = randRange(0, obj.length-1);
      }
      var path = obj[itemId].image;
      
/*
      var change = $(p_obj.target + " li[selID='0']");
      selectedMenu = change;
*/
      
      menuHighLight(itemId);
      imgCall(path, itemId);
      
    }
    
    var slideshow = function(){
      itemId++;
      var thisTarget = self;
      if(itemId > obj.length-1){
        itemId = 0;
      }
      menuHighLight(itemId);
      fade(itemId);
    }
    
    var fade = function(id, inbound){
      ///////////////////////////////////////////////////
      // USED TO PICK WHICH ITEM TO FADE ////////////////
      ///////////////////////////////////////////////////
      
      var fadeTime = 500
      if(inbound == 'click'){
      	fadeTime = 250;
      }
/*
      var change = $(p_obj.target + " li[selID='"+itemId+"']");
      selectedMenu = change;
*/
      if(currFade == 0){
        $(p_obj.target + ' .swfItem').animate({opacity: 0}, fadeTime,function(){
        	$(this).empty();
        	var path  = eval('obj['+itemId+']["image"]');
          var extType = xtractExtension(path);
          
          if(extType.ext != "swf"){
            imgCall(path, id);
          }else{
            swfLoad(path);
          }
        });
      }else{
        var path  = eval('obj['+itemId+']["image"]');
        var extType = xtractExtension(path);
        
        if(extType.ext != "swf"){
          imgCall(path, id);
        }else{
          swfLoad(path);
        }  
      }
    }
    
    var textInfo = function(see){
      ///////////////////////////////////////////////////
      // SCREEN TEXT BAR FADE ON OR OFF /////////////////
      ///////////////////////////////////////////////////
      
      var css;
      if(see == 'off'){
        css = {
          'opacity': '0',
          //'margin'      : (panelH+' 0 0 0'),
          'filter' : 'alpha(opacity=0)'
        }
        $(p_obj.target + ' .infoBar').fadeOut(250);
      }else{
        css = {
          'opacity': '1.0',
          'filter' : 'alpha(opacity=100)'
        }
        if(obj[itemId].header != '' && obj[itemId].desc != ''){
          $(p_obj.target + ' .infoBar').html('<h1>'+obj[itemId].header +'</h1><p>' + obj[itemId].desc+'</p>').fadeIn(250);
        }else{
          css = {
            'opacity': '0',
            'filter' : 'alpha(opacity=0)'
          }
          $(p_obj.target + ' .infoBar').fadeOut(250);
        }
      }
    }
    
    var menuHighLight = function(id){
      ///////////////////////////////////////////////////
      // HIGH LIGHT MENU ////////////////////////////////
      ///////////////////////////////////////////////////
      var change = $(p_obj.target + " li[selID='"+id+"']");
      var currSelect = $(selectedMenu).attr("selID");
      if(selectedMenu != ''){
	      if(p_obj.menuColor != ''){
					$(selectedMenu).removeClass('menuli-on').addClass('menuli');
	      }
      }
      selectedMenu = change;
    	//$(change).css(css);removeClass
			$(change).removeClass('menuli').addClass('menuli-on');
			
    }
  
    var doneLoading = function(id){
      linkItem = obj[id].link;
      
      if(linkItem == ''){
        $(p_obj.target + ' .urlCall').css('display', 'none');
      }else {
        $(p_obj.target + ' .urlCall').css('display', 'block');
      }
      
      var bg = obj[id].image;
      var extType = xtractExtension(bg);
      
      if(extType.ext != "swf"){
        if(p_obj.info != false){
          textInfo('on');
        };
        
        $(p_obj.target + ' .loading').fadeOut(155);
	      if(p_obj.effects == 'slide'){
	      	slideFade(bg);
	      }else{
	      	if(currFade == 0){
	          sliceFadeOut(bg);
	        }else{
	          if(initRotator==false){
	            initFadeOn(bg);
	          }else{
	            sliceFadeOn(bg);
	          }
	        }
        }
      } else{
        //this.swfLoad();
      }
    }  
    var swfLoad = function(path){
      ///////////////////////////////////////////////////
      // SWAPS IMAGE TO SWF /////////////////////////////
      ///////////////////////////////////////////////////
      if(p_obj.info != false){
        textInfo('on');
      };
      
      $(p_obj.target + ' .loading').fadeOut(155);
      $(p_obj.target + ' .swfItem').html('<script type="text/javascript">var EIWeb = new SWFObject("'+obj[itemId].image+'", "'+(p_obj.target+'_flash')+'", "'+panelW+'", "'+panelH+'", "6", "#cccccc");EIWeb.addParam("allowScriptAccess", "sameDomain");EIWeb.addParam("allowFullScreen", "true");EIWeb.addParam("quality", "high");EIWeb.addParam("scale", "noscale");EIWeb.addParam("loop", "false");EIWeb.addParam("menu", "false");EIWeb.addParam("salign", "left");EIWeb.addParam("align", "center");EIWeb.addParam("wmode", "Transparent");EIWeb.write("'+(p_obj.target+'_flash')+'");</script>');
      
      $(p_obj.target + ' .swfItem').animate({opacity: 1}, 1000);
    }
  	var transitionSlide = function(){
      var totalCnt  = obj.length;
      
      
      var buildSlice = '<ul class="ulSlice">';
        for (counter = 0; counter <= totalCnt-1; counter += 1) {

          var jumpX = (panelW*counter);
          
          buildSlice += '<li style="';
          buildSlice += 'position: absolute; white-space: nowrap; top:0; left:'+jumpX+'px; padding: 0px; width: '+panelW+'px; height:'+panelH+'px;"';
          buildSlice += 'tranID="'+counter+'"></li>';
          
          objTrans.push({x:jumpX});
        }
      buildSlice     += '</ul>';
      $(p_obj.target + ' .transition').append(buildSlice);
      
      css = {
        'position'    : 'absolute',
        'list-style-type:' : 'none',
        'white-space' : 'nowrap',
        'margin'      : 0,
        'padding'      : 0,
        'height'			: panelH,
        'width'        : panelW*totalCnt
      };
      
      $(p_obj.target + ' ul.ulSlice').css(css);
  	}
    var transition = function(){
      var breakoutX = p_obj.quality;
      var breakoutY = p_obj.slice;
      var totalCnt  = breakoutX*breakoutY;
      var tranSliceX = Math.round(panelW/breakoutX)+5;
      var tranSliceY = Math.round(panelH/breakoutY)+5;
      var jumpNextY = 0;
      var jumpCount = 0;
      
      
      var buildSlice = '<ul class="ulSlice">';
        for (counter = 0; counter <= totalCnt-1; counter += 1) {
        	var jumpY;
        	if (jumpCount == breakoutX) {
        		jumpCount = 0;
        		jumpNextY += tranSliceY;
        		//console.log('  jumpCount RUN   ' + jumpCount);
        	}
        	
        	//console.log(tranSliceY +'  jumpCount   ' + jumpCount);
          var x=Math.round(0xffffff * Math.random()).toString(16);
          var y=(6-x.length);
          var z="000000";
          var z1 = z.substring(0,y);
          var color= "#" + z1 + x;
          var jumpX = (tranSliceX*jumpCount);
          
          buildSlice += '<li style="';
          buildSlice += 'background-color:#000000; background-image: url('+p_obj.imagePath+'loading.gif); background-repeat: repeat; background-position: '+(-(jumpX))+'px '+(-(jumpNextY))+'px; list-style-type:none; position: absolute; white-space: nowrap; top:'+jumpNextY+'; left: '+jumpX+'px; padding: 0px; width: '+tranSliceX+'px; height:'+tranSliceY+'px;"';
          buildSlice += 'tranID="'+counter+'"></li>';
          
          objTrans.push({x:jumpX, y:jumpNextY});
    			jumpCount++;
        }
      buildSlice     += '</ul>';
      $(p_obj.target + ' .transition').append(buildSlice);
      
      css = {
        'position'    : 'absolute',
        'list-style-type:' : 'none',
        'white-space' : 'nowrap',
        'margin'      : 0,
        'padding'      : 0,
        'width'        : panelW
      };
      $(p_obj.target + ' ul.ulSlice').css(css);
      
    }  
    
    var sliceFadeOut = function(imageLoad){
      ///////////////////////////////////////////////////
      // FADE ITEM OUT /////////////////////////////
      ///////////////////////////////////////////////////
      
      var tempid;
      currFade = 1;
      var items = $(p_obj.target + ' .transition li');
      
      var breakoutX = p_obj.quality;
      var breakoutY = p_obj.slice;
      var itemsCount  = (breakoutX*breakoutY)-1;
      
      var css = {
        'opacity'      : '1',
        'background-image' : ('url('+imageLoad+')'),
        'background-color' : '#000000'
      };
      $(p_obj.target + ' .slide_main').css(css);
      
      if(p_obj.effects == 'random'){
        tempArray = [];
        for (counter = 0; counter <= itemsCount; counter += 1) {
          tempArray.push(counter);
        }
      }
      
      for (counter = 0; counter <= itemsCount; counter += 1) {
        var css = {
          'opacity'      : '1'
        };
        clearTimeout(slideTime);
        tempid = counter;
        
        if(p_obj.effects == 'random'){
          var randId = randRange(0, tempArray.length-1);
          if(tempArray.length <= 1){
            randId = 0;
          }
          tempid = tempArray.splice(randId, 1);
        }
        
        $(items[tempid]).css(css);
        
        if(counter != itemsCount){
          transType(items, counter, 0, false, 'off');
        } else{
          transType(items, counter, 0, true, 'off');
        }
      }
    }
  
    var sliceFadeOn = function(imageLoad){
      ///////////////////////////////////////////////////
      // FADE ITEM IN ///////////////////////////////////
      ///////////////////////////////////////////////////
      
      var tempid;
      var items = $(p_obj.target + ' .transition li');
      currFade = 0;
      var breakoutX = p_obj.quality;
      var breakoutY = p_obj.slice;
      var itemsCount  = (breakoutX*breakoutY)-1;
      clearTimeout(slideTime);
      
      if(p_obj.effects == 'random'){
        tempArray = [];
        for (counter = 0; counter <= itemsCount; counter += 1) {
          tempArray.push(counter);
        }
      }
      
      for (var counter = 0; counter <= itemsCount; counter += 1) {
        
        var css = {
          'background-image'    : 'url('+imageLoad+')',
          'opacity'      : '0'
        };
        tempid = counter;
        if(p_obj.effects == 'random'){
          var randId = randRange(0, tempArray.length-1);
          if(tempArray.length <= 1){
            randId = 0;
          }
          tempid = tempArray.splice(randId, 1);
        }
        $(items[tempid]).css(css);
        
        if(counter != itemsCount){
          transType(items, counter, 1, false, 'on');
        } else{
          transType(items, counter, 1, true, 'on');
        }
        
      }
    }  
    var initFadeOn = function(imageLoad){
      ///////////////////////////////////////////////////
      // FIRST FADE WHEN PLUGIN INIT ////////////////////
      ///////////////////////////////////////////////////
      
      currFade = 0;
      var tempid;
      var items = $(p_obj.target + ' .transition li');
      var breakoutX = p_obj.quality;
      var breakoutY = p_obj.slice;
      var itemsCount  = (breakoutX*breakoutY)-1;
      
      var css = {
        'opacity'      : '1'
      };
      $(p_obj.target + ' .transition').css(css);
      
      if(p_obj.effects == 'random' || p_obj.effects == 'explode'){
        tempArray = [];
        for (counter = 0; counter <= itemsCount; counter += 1) {
          tempArray.push(counter);
        }
      }
      
      for (var counter = 0; counter <= itemsCount; counter += 1) {
        
        var css = {
          'background-image'    : 'url('+imageLoad+')',
          'opacity'      : '0'
        };
        
        tempid = counter;
        
        var newX; 
        var newY;
          
        var posX = objTrans[counter]['x']; 
        var posY = objTrans[counter]['y'];
        
        if(p_obj.effects == 'explode'){
          newX = randRange(-panelW,  (panelW+300));
          newY = randRange(-panelH,  (panelH+300));
        }
        if(p_obj.slice == 1){
        	newY = posY;
        } else if(p_obj.quality == 1){
        	newX = posX;
        }
	      css = {
	      	'background-image' : 'url('+imageLoad+')',
	        'opacity': '0',
					'top' : newY,
	        'left': newX
	      }
        
        
        if(p_obj.effects == 'random' || p_obj.effects == 'explode'){
          var randId = randRange(0, tempArray.length-1);
          if(tempArray.length <= 1){
            randId = 0;
          }
          tempid = tempArray.splice(randId, 1);
          
        }
        $(items[tempid]).css(css);
        
        if(counter != itemsCount){
          transType(items, counter, 1, false, 'on')
        } else{
          transType(items, counter, 1, true, 'on');
        }
      }
      
      initRotator = true;
    }
    var slideFade = function(imageLoad){
			var amt = objTrans[itemId]['x'];
			var items = $(p_obj.target + ' .transition li');
			var tranSpeed = p_obj.effectTime;
			var css = {
      	'opacity': 1,
        'background-image'    : 'url('+imageLoad+')'
      }
      $(items[itemId]).css(css);
      css = {
        'opacity'      : '1'
      };
      $(p_obj.target + ' .transition').css(css);
      css = {
      	
        'opacity': 1,
        'left'    : ((-(amt))+'px')
      }
      $(p_obj.target + ' ul.ulSlice').stop().animate(css, tranSpeed, function () {
        clearTimeout(slideTime);
        slideTime = setTimeout(function () { slideshow(); }, (p_obj.speed*1000));
      });
    }
    var transType = function(item, id, amount, last, type){
      ///////////////////////////////////////////////////
      // TRANSITION CONTROL TYPE ////////////////////////
      ///////////////////////////////////////////////////
			var posX = objTrans[id]['x']; 
			var posY = objTrans[id]['y'];
			
			var newX; 
			var newY;
          
      var delayHack;
      var tranSpeed = p_obj.effectTime;
      delayHack = (amount == 0) ? 1 : 0;  
  
      switch(p_obj.effects){
        case 'fade':
          //FADES THE BARS ON ACROSS
          
          var css = {
            'opacity': amount
          }
          if(!last){
            $(item[id]).stop().animate({opacity: delayHack}, 100*id).animate(css, tranSpeed);
          } else{
            $(item[id]).stop().animate({opacity: delayHack}, 100*id).animate(css, tranSpeed, function () {
              clearTimeout(slideTime);
              if(obj.length > 1){
              	slideTime = setTimeout(function () { slideshow(); }, (p_obj.speed*1000));
              }
            });
          }
        break;        
        case 'blinds':
          //FADES THE BARS ON THE Y
          
          var amt;
          if(type == 'off'){
            amt = panelW;
            //amt = panelH+'px';
          } else{
            //amt = '0px';
            $(item[id]).css({'left':-(panelW+100)});
            amt = 0;      
          }
          var css = {
            'opacity': amount,
            'left' : amt
          }
          if(!last){
            $(item[id]).stop().animate({opacity: delayHack}, p_obj.effectDelaySpeed*id).animate(css, tranSpeed);
          } else{
            $(item[id]).stop().animate({opacity: delayHack}, p_obj.effectDelaySpeed*id).animate(css, tranSpeed, function () {
              clearTimeout(slideTime);
              if(obj.length > 1){
              	slideTime = setTimeout(function () { slideshow(); }, (p_obj.speed*1000));
              }
            });
          }
        break;
        case 'explode':
          //FADES THE BARS RANDOMLY ON THE Y
          
          if(type == 'off'){
            newX = randRange(-panelW,  (panelW+300));
            newY = randRange(-panelH,  (panelH+300));
          } else{
            newX = posX;
            newY = posY;     
          }
            if(p_obj.slice == 1){
            	newY = posY;
            } else if(p_obj.quality == 1){
            	newX = posX;
            }
          var css = {
            'opacity': amount,
						'top' : newY,
            'left': newX
          }
          if(!last){
            $(item[id]).stop().animate({opacity: delayHack}, 50*randRange(1,  p_obj.effectDelaySpeed)).animate(css, tranSpeed);
          } else{
            $(item[id]).stop().animate({opacity: delayHack}, 50*randRange(1,  p_obj.effectDelaySpeed)).animate(css, tranSpeed, function () {
              clearTimeout(slideTime);
              if(obj.length > 1){
              	slideTime = setTimeout(function () { slideshow(); }, (p_obj.speed*1000));
              }
               
            });
          }
        break;
        case 'random':
          //FADES THE BARS RANDOMLY ON THE Y
          
          var amt;
          if(type == 'off'){
            //amt = objTrans[id]['out'];
            amt = panelH+'px';
          } else{
            amt = '0px';
            //amt = objTrans[id]['on'];      
          }
          
          var css = {
            'opacity': amount,
            'top' : amt
          }
          if(!last){
            $(item[id]).stop().animate({opacity: delayHack}, 50*randRange(1,  p_obj.effectDelaySpeed)).animate(css, tranSpeed);
          } else{
            $(item[id]).stop().animate({opacity: delayHack}, 50*randRange(1,  p_obj.effectDelaySpeed)).animate(css, tranSpeed, function () {
              clearTimeout(slideTime);
              if(obj.length > 1){
              	slideTime = setTimeout(function () { slideshow(); }, (p_obj.speed*1000));
              }
               
            });
          }
        break;
        case 'randomFade':
          //FADES THE BARS RANDOMLY
          
          var css = {
            'opacity': amount
          }
          if(!last){
            $(item[id]).stop().animate({opacity: delayHack}, 25*randRange(1,  p_obj.effectDelaySpeed)).animate(css, tranSpeed);
          } else{
            $(item[id]).stop().animate({opacity: delayHack}, 25*randRange(1,  p_obj.effectDelaySpeed)).animate(css, tranSpeed, function () {
              clearTimeout(slideTime);
              if(obj.length > 1){
              	slideTime = setTimeout(function () { slideshow(); }, (p_obj.speed*1000));
              }
               
            });
          }
        break;
      }
      
    }
    function randRange(minNum, maxNum) 
    {
      ///////////////////////////////////////////////////
      // RANDOM NUMBER FROM A RANGE /////////////////////
      ///////////////////////////////////////////////////
      var ranNum = Math.floor(Math.random() * maxNum - minNum + 1) + minNum;
      return ranNum;
    }
    
    function xtractExtension(data){
      ///////////////////////////////////////////////////
      // STRIPS THE EXTENSION OFF THE FILE //////////////
      ///////////////////////////////////////////////////
      data = data.replace(/^\s|\s$/g, ""); //trims string
    
      if (/\.\w+$/.test(data)) {
          if (data.match(/([^\/\\]+)\.(\w+)$/) )
              return {filename: RegExp.$1, ext: RegExp.$2};
          else
              return {filename: "no file name", ext:null};
      }
      else {
          if (data.match(/([^\/\\]+)$/) )
              return {filename: RegExp.$1, ext: null};
          else
              return {filename: "no file name", ext:null};
      }
    }
    settings();
  }
})(jQuery);